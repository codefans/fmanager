package main.java.com.fmanager.models;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class UserAdditionDetial extends User {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long userId;
	
	
	public UserAdditionDetial(String username, String password, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
		// TODO Auto-generated constructor stub
	}
	
	public UserAdditionDetial(Long userid , String username, String password, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
		this.userId = userid;
	}

	public long getUserId() {
		return userId;
	}

}

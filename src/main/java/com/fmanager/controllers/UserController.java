package main.java.com.fmanager.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import main.java.com.fmanager.exception.FmanagerRestException;
import main.java.com.fmanager.models.User;
import main.java.com.fmanager.services.UserServcie;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserServcie userService;

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<OAuth2AccessToken> register(@RequestBody User user) throws FmanagerRestException {
		OAuth2AccessToken token = userService.registerUser(user);
		HttpHeaders headers = new HttpHeaders();
		headers.set("Cache-Control", "no-store");
		headers.set("Pragma", "no-cache");
		return new ResponseEntity<OAuth2AccessToken>(token, headers, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public User getUserById(@PathVariable long id) {
		return userService.findById(id);
	}
	
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteUserById(@PathVariable long id) {
		userService.deleteUserById(id);
	}
	
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<User> getUserList(){
		return userService.getUserList();
	}

}

package main.java.com.fmanager.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import main.java.com.fmanager.models.User;


@Mapper
public interface UserDAO {
	public User findById(long id);
	
	public User findByUserName(String name);

	public User findByEmail(String email);

	public void registerUser(User user);

	public List<User> getUserList();

	public void deleteUserById(long id);
}

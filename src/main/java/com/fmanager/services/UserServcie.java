package main.java.com.fmanager.services;

import java.util.List;

import org.springframework.security.oauth2.common.OAuth2AccessToken;

import main.java.com.fmanager.exception.FmanagerRestException;
import main.java.com.fmanager.models.User;

public interface UserServcie {
	
	public User findById(long id);

	public OAuth2AccessToken registerUser(User user) throws FmanagerRestException;
	
	public List<User> getUserList();

	public void deleteUserById(long id);
	
}

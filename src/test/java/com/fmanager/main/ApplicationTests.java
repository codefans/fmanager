package test.java.com.fmanager.main;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class ApplicationTests {

    public void contextLoads() {
    }

    public static void main(String[] args) {

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        System.out.println(encoder.encode("c91c03ea6c46a86cbc019be3d71d0a1a")); //$NON-NLS-1$
        
        System.out.println(encoder.matches("c91c03ea6c46a86cbc019be3d71d0a1a", "$2a$10$9OjxOxePUEjU694FF5DaxOrdpihrC7qnuarroosbmqgzT5/ysfW3i"));
        
    }

}

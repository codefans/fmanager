CREATE TABLE IF NOT EXISTS user_role (
  id SERIAL PRIMARY KEY,
  description varchar(64) DEFAULT NULL,
  role_name varchar(64) DEFAULT NULL
);

--INSERT INTO user_role (role_name, description) VALUES ( 'STANDARD_USER', 'Standard User - Has no admin rights');
--INSERT INTO user_role (role_name, description) VALUES ( 'ADMIN_USER', 'Admin User - Has permission to perform admin tasks');